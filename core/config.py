import os
from functools import cached_property
from pathlib import Path

import yaml

CONFIG_FILE = Path(__file__).parents[1] / 'config.yaml'


class ConfigType:
    @cached_property
    def config(self):
        with open(CONFIG_FILE) as f:
            return yaml.load(f, Loader=yaml.FullLoader)

    def __getattr__(self, item):
        return os.environ.get(item, self.config.get(item))


Config = ConfigType()
