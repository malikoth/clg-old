from datetime import datetime, timezone
from functools import cached_property

from django.db import models


class AutoListSerializer:
    @cached_property
    def list_serializer(self):
        if hasattr(self, 'list_action_serializer_class'):
            return self.list_action_serializer_class

        if hasattr(self, 'list_action_fields'):
            Meta = type('Meta', (self.serializer_class.Meta,), {'fields': self.list_action_fields})
            serializer_class = type('AutoListSerializer', (self.serializer_class,), {'Meta': Meta})
            return serializer_class

        if hasattr(self, 'list_action_excludes'):
            current_fields = self.serializer_class().fields
            fields = tuple(set(current_fields) - set(self.list_action_excludes))
            Meta = type('Meta', (self.serializer_class.Meta,), {'fields': fields})
            serializer_class = type('AutoListSerializer', (self.serializer_class,), {'Meta': Meta})
            return serializer_class

        return self.serializer_class

    def get_serializer_class(self):
        if self.action == 'list':
            return self.list_serializer
        return self.serializer_class


class FormatStringMixin:
    def __str__(self):
        return self.format_string.format(**vars(self))


class DeletedManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(deleted=None)


class ManagedModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(null=True)
    deleted = models.DateTimeField(null=True)

    objects = DeletedManager()
    all_objects = models.Manager()

    def delete(self):
        self.deleted = datetime.now(timezone.utc)
        self.save()

    def undelete(self):
        self.deleted = None
        self.save()

    class Meta:
        abstract = True
        ordering = ['id']
