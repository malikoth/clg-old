from enum import Enum, IntEnum  # noqa: F401
from functools import wraps
from IPy import IP, IPSet


class classproperty:
    def __init__(self, func):
        self.func = func

    def __get__(self, obj, objtype=None):
        if objtype is None:
            objtype = type(obj)
        return wraps(self.func)(self.func)(objtype)


class StringIPSet(IPSet):
    def __init__(self, collection):
        collection = [IP(address) if isinstance(address, str) else address for address in collection]
        super().__init__(collection)

    def __contains__(self, address):
        if isinstance(address, str):
            address = IP(address)
        return super().__contains__(address)


class StrEnum(str, Enum):
    def __str__(self):
        return str(self.value)

    def __eq__(self, other):
        return str(self).lower() == str(other).lower()

    def __hash__(self):
        return hash(str(self))


class ChoicesEnum(Enum):
    """An Enum class where the value is a tuple of (short) canonicalized value, and natural language description"""
    def __init__(self, serial: str, description: str):
        self.serial = serial
        self.description = description

    @classproperty
    def choices(cls):
        for element in cls:
            yield element.value

    def __str__(self):
        return str(self.value)

    def __eq__(self, other):
        return str(self).lower() == str(other).lower()

    def __hash__(self):
        return hash(str(self))
