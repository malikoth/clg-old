from django.urls import path
from rest_framework.routers import DefaultRouter

from .models import ChecklistViewSet

router = DefaultRouter()
router.register('checklists', ChecklistViewSet)


# app_name = 'checklist'
urlpatterns = router.urls
