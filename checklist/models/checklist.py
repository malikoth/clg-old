import logging

from django.db import models
from rest_framework import serializers, viewsets

from core.utils import StrEnum
from core.django_utils import AutoListSerializer


log = logging.getLogger(__name__)


class CheckboxStatus(StrEnum):
    UNCHECKED = 'unchecked'
    CHECKED = 'checked'
    INDETERMINATE = 'indeterminate'


class Checklist(models.Model):
    name = models.CharField(max_length=256)
    description = models.CharField(max_length=256)


class ChecklistSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Checklist
        fields = '__all__'


class ChecklistViewSet(AutoListSerializer, viewsets.ModelViewSet):
    queryset = Checklist.objects.all()
    serializer_class = ChecklistSerializer
    # list_action_fields = ['name']
    list_action_excludes = ['description']
    # list_action_serializer_class = ChecklistListSerializer
